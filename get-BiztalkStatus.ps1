<#
    dependency: biztalk management software installed on server
    check biztalk
    https://social.technet.microsoft.com/wiki/contents/articles/35011.powershell-script-to-get-the-status-of-all-biztalk-artifacts.aspx 

    Get-ReceiveLocationStatus
    Get-SendPortStatus
    Get-OrchestrationStatus
    Get-HostsStatus
    Get-ApplicationStatus
 
#>

#Set-ExecutionPolicy Unrestricted
  
# Import assembly
[void] [System.reflection.Assembly]::LoadWithPartialName("Microsoft.BizTalk.ExplorerOM")
$server = '<biztalkmessagedbserver>'

#create a new object
$Catalog = New-Object Microsoft.BizTalk.ExplorerOM.BtsCatalogExplorer
  
#BizTalk Config
$Catalog.ConnectionString = "SERVER=s$server;DATABASE=BizTalkMgmtDb;Integrated Security=SSPI"  #Connection string to BizTalk Mgmt database



function Get-ReceiveLocationStatus()
{
    write-host ""
    write-host ""
  
  "{0,-50} {1,-20} " -f ` "Receive Location","Status"
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
  
   foreach ($receivePort in $catalog.ReceivePorts)
   {
       foreach($receiveLoc in $receivePort.ReceiveLocations)
        {
           "{0,-50} {1,-20} " -f ` $receiveLoc.Name, $receiveLoc.Enable   
        }
         
   }
}

Function Get-OrchestrationStatus()
{
  
  Write-Host ""
  Write-Host ""
  
  "{0,-80} {1,-20} " -f ` "Orchestration","Status"
  
  "{0,-80} {1,-20} " -f ` "--------------------","----------"
  
    foreach($assembly in $catalog.Assemblies)
    {
        foreach($orch in $assembly.Orchestrations)
        {
           "{0,-80} {1,-20} " -f ` $orch.Fullname,$orch.Status
        }
    }
}

function Get-ApplicationStatus()
{
    write-host ""
    write-host ""
  
  "{0,-50} {1,-20} " -f ` "Application","Status"
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
   foreach ($app in $catalog.Applications)
   {
       "{0,-50} {1,-20} " -f ` $app.Name,$app.Status
   }
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
}



function Get-SendPortStatus()
{
    write-host ""
    write-host ""
  
  "{0,-50} {1,-20} " -f ` "Send Port","Status"
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
   foreach ($sendport in $catalog.SendPorts)
   {
     "{0,-50} {1,-20} " -f ` $sendPort.Name,$sendPort.Status
   }
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
}

function Get-HostsStatus()
{
    write-host ""
    write-host ""
  
  "{0,-50} {1,-20} " -f ` "Host Instance","Status"
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
  
   $hosts = Get-WmiObject MSBTS_HostInstance -namespace 'root/MicrosoftBizTalkServer'
   foreach ($hostinst in $hosts)
   {
      "{0,-50} {1,-20} " -f ` $hostinst.HostName,$hostinst.ServiceState
   }
  "{0,-50} {1,-20} " -f ` "--------------------","----------"
}


#generate output 

    Get-ReceiveLocationStatus | out-file .\report2.txt
    Get-SendPortStatus  | out-file .\report2.txt -append
    Get-OrchestrationStatus  | out-file .\report2.txt -append
    Get-HostsStatus  | out-file .\report2.txt -append
    Get-ApplicationStatus  | out-file .\report2.txt -append

    invoke-item .\report2.txt

